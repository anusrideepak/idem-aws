import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_nodegroup(
    hub,
    ctx,
    aws_iam_worker_role_assignment,
    aws_ec2_subnet_for_eks_worker,
    aws_eks_cluster,
):

    if hub.tool.utils.is_running_localstack(ctx):
        return

    cluster_name = aws_eks_cluster.get("name")

    # Describe cluster
    describe_ret = await hub.states.aws.eks.cluster.describe(ctx)
    assert cluster_name in describe_ret

    # node group creation
    node_group_name = "idem-test-nodegroup-1"
    worker_arn = aws_iam_worker_role_assignment["arn"]
    ret = await hub.states.aws.eks.nodegroup.present(
        ctx,
        name=node_group_name,
        cluster_name=cluster_name,
        version="1.21",
        release_version="1.21.5-20220123",
        capacity_type="ON_DEMAND",
        ami_type="AL2_x86_64",
        node_role=worker_arn,
        instance_types=["t1.micro"],
        subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
        disk_size=20,
        scaling_config={"desiredSize": 2, "maxSize": 2, "minSize": 2},
        update_config={"maxUnavailable": 1},
        tags={"Name": node_group_name},
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert ret["comment"] == f"Created '{node_group_name}'"
    resource = ret.get("new_state")
    node_group_id = resource.get("nodegroupName")

    # Describe node group
    describe_ret = await hub.states.aws.eks.nodegroup.describe(ctx)
    assert node_group_id in describe_ret

    # TODO: update version API is taking time to execute and it is linked with cluster version. need to fix this to make it work end to end
    # update version
    # ret = await hub.states.aws.eks.nodegroup.present(
    #     ctx,
    #     name=node_group_name,
    #     cluster_name=cluster_name,
    #     version="1.21",
    #     release_version="1.21.5-20220123",
    #     capacity_type="ON_DEMAND",
    #     ami_type="AL2_x86_64",
    #     node_role=worker_arn,
    #     instance_types=["t1.micro"],
    #     subnets=[aws_ec2_subnet_for_eks_worker.get("resource_id")],
    #     disk_size=20,
    #     scaling_config={"desiredSize": 2, "maxSize": 2, "minSize": 2},
    #     update_config={"maxUnavailable": 1},
    #     tags={"Name": node_group_name},
    # )
    # assert ret["result"], ret["comment"]
    # assert ret.get("old_state") and ret.get("new_state")
    # assert ret["comment"] == f"Updated '{node_group_name}'"

    # Describe node group
    # describe_ret = await hub.states.aws.eks.nodegroup.describe(ctx)
    # assert node_group_id in describe_ret
    # resource = describe_ret.get(node_group_id).get("aws.eks.nodegroup.present")
    # describe_version = None
    # for item in resource:
    #     if "version" in item:
    #         describe_version = item.get("version")
    # assert describe_version == "1.21"

    # Delete node group
    ret = await hub.states.aws.eks.nodegroup.absent(
        ctx, name=node_group_id, cluster_name=cluster_name
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert ret["comment"] == f"Deleted '{node_group_id}'"
