import copy
import uuid

import pytest


@pytest.mark.localstack(pro=False)
@pytest.mark.asyncio
async def test_eks_fargate_profile(
    hub, ctx, aws_eks_cluster_fargate, aws_iam_cluster_fargate_role_assignment
):
    if hub.tool.utils.is_running_localstack(ctx):
        return

    selectors = [
        {
            "namespace": "default",
        },
    ]
    cluster_name = aws_eks_cluster_fargate.get("name")
    fargate_profile_name = "idem-test-fargate-profile-" + str(uuid.uuid4())
    resource_id = fargate_profile_name + "-" + cluster_name
    pod_arn = aws_iam_cluster_fargate_role_assignment["arn"]

    # create fargate profile with test flag
    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True
    ret = await hub.states.aws.eks.fargate_profile.present(
        test_ctx,
        name=fargate_profile_name,
        resource_id=resource_id,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert fargate_profile_name == ret.get("new_state").get("name")
    assert (
        f"Would create aws.eks.fargate_profile '{fargate_profile_name}'"
        in ret["comment"]
    )

    # create fargate profile on real aws env and no resource_id
    ret = await hub.states.aws.eks.fargate_profile.present(
        ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and ret.get("new_state")
    assert f"Created aws.eks.fargate_profile '{fargate_profile_name}'" in ret["comment"]

    # create existing resource with resource_id on a real aws env
    ret = await hub.states.aws.eks.fargate_profile.present(
        ctx,
        name=fargate_profile_name,
        resource_id=resource_id,
        cluster_name=cluster_name,
        pod_execution_role_arn=pod_arn,
        selectors=selectors,
    )

    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and ret.get("new_state")
    assert (
        f"aws.eks.fargate_profile '{fargate_profile_name}' already exists"
        in ret["comment"]
    )

    # describe resource
    ret = await hub.states.aws.eks.fargate_profile.describe(ctx)
    assert resource_id in ret
    assert "aws.eks.fargate_profile.present" in ret.get(resource_id)

    # delete resource with test flag
    ret = await hub.states.aws.eks.fargate_profile.absent(
        test_ctx,
        name=fargate_profile_name,
        cluster_name=cluster_name,
        resource_id=resource_id,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert (
        f"Would delete aws.eks.fargate_profile '{fargate_profile_name}'"
        in ret["comment"]
    )

    # delete resource on real aws env
    ret = await hub.states.aws.eks.fargate_profile.absent(
        ctx,
        resource_id=resource_id,
        name=fargate_profile_name,
        cluster_name=cluster_name,
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    assert f"Deleted aws.eks.fargate_profile '{fargate_profile_name}'" in ret["comment"]

    # delete an already existing resource
    ret = await hub.states.aws.eks.fargate_profile.absent(
        ctx,
        resource_id=resource_id,
        name=fargate_profile_name,
        cluster_name=cluster_name,
    )
    assert ret["result"], ret["comment"]
    assert not ret.get("old_state") and not ret.get("new_state")
    assert f"'{fargate_profile_name}' already absent" in ret["comment"]
