"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__

hub.exec.boto3.client.eks.create_fargate_profile
hub.exec.boto3.client.eks.delete_fargate_profile
hub.exec.boto3.client.eks.describe_fargate_profile
hub.exec.boto3.client.eks.list_fargate_profiles
"""
import copy
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["resource"]

default_attempts = 120

TREQ = {
    "absent": {
        "require": [
            "aws.ec2.subnet.absent",
            "aws.ec2.vpc.absent",
            "aws.iam.role.absent",
            "aws.iam.role_policy_attachment.absent",
            "aws.eks.cluster.absent",
        ],
    },
    "present": {
        "require": [
            "aws.ec2.subnet.present",
            "aws.ec2.vpc.present",
            "aws.iam.role.present",
            "aws.iam.role_policy_attachment.present",
            "aws.eks.cluster.present",
        ],
    },
}


async def present(
    hub,
    ctx,
    name: str,
    cluster_name: str,
    pod_execution_role_arn: str,
    resource_id: str = None,
    subnets: List = None,
    selectors: List = None,
    client_request_token: str = None,
    tags: Dict = None,
) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Creates an Fargate profile for your Amazon EKS cluster. You must have at least one Fargate profile in a cluster
    to be able to run pods on Fargate. The Fargate profile allows an administrator to declare which pods run on
    Fargate and specify which pods run on which Fargate profile. This declaration is done through the profile’s
    selectors. Each profile can have up to five selectors that contain a namespace and labels. A namespace is
    required for every selector. The label field consists of multiple optional key-value pairs. Pods that match the
    selectors are scheduled on Fargate. If a to-be-scheduled pod matches any of the selectors in the Fargate
    profile, then that pod is run on Fargate. When you create a Fargate profile, you must specify a pod execution
    role to use with the pods that are scheduled with the profile. This role is added to the cluster's Kubernetes
    Role Based Access Control (RBAC) for authorization so that the kubelet that is running on the Fargate
    infrastructure can register with your Amazon EKS cluster so that it can appear in your cluster as a node. The
    pod execution role also provides IAM permissions to the Fargate infrastructure to allow read access to Amazon
    ECR image repositories. For more information, see Pod Execution Role in the Amazon EKS User Guide. Fargate
    profiles are immutable. However, you can create a new updated profile to replace an existing profile and then
    delete the original after the updated profile has finished creating. If any Fargate profiles in a cluster are in
    the DELETING status, you must wait for that Fargate profile to finish deleting before you can create any other
    profiles in that cluster. For more information, see Fargate Profile in the Amazon EKS User Guide.

    Args:
        name(Text): A name, ID to identify the resource.
        cluster_name(Text): The name of the Amazon EKS cluster to apply the Fargate profile to.
        pod_execution_role_arn(Text): The Amazon Resource Name (ARN) of the pod execution role to use for pods that match the
            selectors in the Fargate profile. The pod execution role allows Fargate infrastructure to
            register with your cluster as a node, and it provides read access to Amazon ECR image
            repositories. For more information, see Pod Execution Role in the Amazon EKS User Guide.
        subnets(List, optional): The IDs of subnets to launch your pods into. At this time, pods running on Fargate are not
            assigned public IP addresses, so only private subnets (with no direct route to an Internet
            Gateway) are accepted for this parameter. Defaults to None.
        selectors(List, optional): The selectors to match for pods to use this Fargate profile. Each selector must have an
            associated namespace. Optionally, you can also specify labels for a namespace. You may specify
            up to five selectors in a Fargate profile. Defaults to None.
        client_request_token(Text, optional): Unique, case-sensitive identifier that you provide to ensure the idempotency of the request. Defaults to None.
        tags(Dict, optional): The metadata to apply to the Fargate profile to assist with categorization and organization.
            Each tag consists of a key and an optional value. You define both. Fargate profile tags do not
            propagate to any other resources associated with the Fargate profile, such as the pods that are
            scheduled with it. Defaults to None.
        resource_id(Text, optional): ID (combination of name and cluster_name) to identify the resource

        Request Syntax:
            fargate_resource_id:
               aws.eks.fargate_profile.present:
               - name: 'string'
               - resource_id: 'string'
               - cluster_name: 'string'
               - pod_execution_role_arn: 'string'
               - subnets:
                  - private_subnet_id
                  - private_subnet_id
            - selectors:
                   - labels: {}
                     namespace: 'string'
            Note: Private subnets selected for fargate profile creation must be in different availability zones


    Returns:
        Dict[str, Any]

    Examples:
        .. code-block:: sls

            fargate01-idem-cluster01:
              aws.eks.fargate_profile.present:
              - name: fargate01
              - cluster_name: idem-cluster01
              - pod_execution_role_arn: arn:aws:iam::537227425989:role/eksfargateprofile-role
              - subnets:
                - subnet-006a41c410b9485dd
                - subnet-01368936bb540ba1e
              - selectors:
                - labels: {}
                  namespace: default
              - tags:
                  fargate01: 'created via code'
    """

    result = dict(comment=(), old_state=None, new_state=None, name=name, result=True)
    resource_created = False

    # resource id would be name-clusterName to maintain integrity as same fargate profile names can exist in different clusters
    # passing name(unique id for fargate profile) in apis
    before = await hub.exec.boto3.client.eks.describe_fargate_profile(
        ctx, clusterName=cluster_name, fargateProfileName=name
    )
    if before:
        result[
            "old_state"
        ] = hub.tool.aws.eks.conversion_utils.convert_raw_fargate_profile_to_present(
            raw_resource=before["ret"]["fargateProfile"]
        )
        result["new_state"] = copy.deepcopy(result["old_state"])
        result["comment"] = (f"aws.eks.fargate_profile '{name}' already exists",)
        # fargate_profile cannot be updated except for tags after its creation, tag update apis are currently
        # supported for eks clusters and nodegroups
        return result
    else:
        if ctx.get("test", False):
            result["new_state"] = hub.tool.aws.test_state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "cluster_name": cluster_name,
                    "pod_execution_role_arn": pod_execution_role_arn,
                    "tags": tags,
                    "subnets": subnets,
                    "selectors": selectors,
                },
            )
            result["comment"] = (f"Would create aws.eks.fargate_profile '{name}'",)
            return result
        try:
            ret = await hub.exec.boto3.client.eks.create_fargate_profile(
                ctx,
                fargateProfileName=name,
                clusterName=cluster_name,
                podExecutionRoleArn=pod_execution_role_arn,
                subnets=subnets,
                selectors=selectors,
                clientRequestToken=client_request_token,
                tags=tags,
            )
            result["result"] = ret["result"]
            if not result["result"]:
                result["comment"] = ret["comment"]
                return result

            # wait till fargate_profile is active
            await hub.tool.boto3.client.wait(
                ctx,
                "eks",
                "fargate_profile_active",
                None,
                clusterName=cluster_name,
                fargateProfileName=name,
                WaiterConfig={"MaxAttempts": default_attempts},
            )
            resource_created = True
            resource_id = name
            result["comment"] = (f"Created aws.eks.fargate_profile '{name}'",)
        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)
            result["result"] = False
    try:
        if resource_created:
            after = await hub.exec.boto3.client.eks.describe_fargate_profile(
                ctx, clusterName=cluster_name, fargateProfileName=resource_id
            )
            result[
                "new_state"
            ] = hub.tool.aws.eks.conversion_utils.convert_raw_fargate_profile_to_present(
                raw_resource=after["ret"]["fargateProfile"]
            )
    except Exception as e:
        result["comment"] = result["comment"] + (str(e),)
        result["result"] = False
    return result


async def absent(
    hub, ctx, name: str, resource_id: str, cluster_name: str
) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    Deletes an Fargate profile. When you delete a Fargate profile, any pods running on Fargate that were created
    with the profile are deleted. If those pods match another Fargate profile, then they are scheduled on Fargate
    with that profile. If they no longer match any Fargate profiles, then they are not scheduled on Fargate and they
    may remain in a pending state. Only one Fargate profile in a cluster can be in the DELETING status at a time.
    You must wait for a Fargate profile to finish deleting before you can delete any other profiles in that cluster.

    Args:
        resource_id(Text): ID (combination of name and cluster_name) to identify the resource
        name(Text): A name, ID to identify the resource.
        cluster_name(Text): The name of the Amazon EKS cluster associated with the Fargate profile to delete.

    Request Syntax:
        fargate_resource_id:
           aws.eks.fargate_profile.absent:
           - resource_id: 'string'
           - name: 'string'
           - cluster_name: 'string'
    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls
            fargate01:
              aws.eks.fargate_profile.absent:
              - resource_id: fargate01-idem-cluster01
              - name: fargate01
              - cluster_name: idem-cluster01
    """

    result = dict(comment=(), old_state=None, new_state=None, name=name, result=True)
    try:
        unique_id = resource_id.split("-" + cluster_name)[0]
    except Exception:
        result["comment"] = (f"resource_id is mandatory for absent function",)
        return result
    before = await hub.exec.boto3.client.eks.describe_fargate_profile(
        ctx, clusterName=cluster_name, fargateProfileName=unique_id
    )

    if not before:
        result["comment"] = (f"'{name}' already absent",)
    elif ctx.get("test", False):
        result[
            "old_state"
        ] = hub.tool.aws.eks.conversion_utils.convert_raw_fargate_profile_to_present(
            raw_resource=before["ret"]["fargateProfile"]
        )
        result["comment"] = (f"Would delete aws.eks.fargate_profile '{name}'",)
        return result
    else:
        result[
            "old_state"
        ] = hub.tool.aws.eks.conversion_utils.convert_raw_fargate_profile_to_present(
            raw_resource=before["ret"]["fargateProfile"]
        )
        try:
            ret = await hub.exec.boto3.client.eks.delete_fargate_profile(
                ctx, clusterName=cluster_name, fargateProfileName=unique_id
            )
            result["result"] = ret["result"]
            if not result["result"]:
                result["comment"] = ret["comment"]
                result["result"] = False
                return result
            await hub.tool.boto3.client.wait(
                ctx,
                "eks",
                "fargate_profile_deleted",
                None,
                fargateProfileName=unique_id,
                clusterName=cluster_name,
                WaiterConfig={"MaxAttempts": default_attempts},
            )
            result["comment"] = (f"Deleted aws.eks.fargate_profile '{name}'",)
        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)

    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function
    Lists the Fargate profiles associated with the specified cluster in your Amazon Web Services account in the
    specified Region.
    Returns:
        Dict[str, Any]
    Examples:

        .. code-block:: bash

            $ idem describe aws.eks.fargate_profile
    """

    result = {}
    try:
        cluster_ret = await hub.exec.boto3.client.eks.list_clusters(ctx)
        if not cluster_ret["result"]:
            hub.log.debug(f"Could not list clusters {cluster_ret['comment']}")
            return {}

        fargate_profile_list = []
        for clusterName in cluster_ret["ret"]["clusters"]:
            fargate_profiles_ret = (
                await hub.exec.boto3.client.eks.list_fargate_profiles(
                    ctx, clusterName=clusterName
                )
            )
            if not fargate_profiles_ret["result"]:
                result["comment"] = fargate_profiles_ret["comment"]
                return result

            # save cluster name and fargate profile name to describe fargate profiles
            for fargate_profile_name in fargate_profiles_ret["ret"][
                "fargateProfileNames"
            ]:
                fargate_profile_list.append(
                    {
                        "clusterName": clusterName,
                        "resource_id": fargate_profile_name,
                    }
                )
        for fargate_profile in fargate_profile_list:
            clusterName = fargate_profile["clusterName"]
            resource_id = fargate_profile["resource_id"]
            describe_ret = await hub.exec.boto3.client.eks.describe_fargate_profile(
                ctx, clusterName=clusterName, fargateProfileName=resource_id
            )

            fargate_profile = describe_ret["ret"]["fargateProfile"]
            translated_resource = hub.tool.aws.eks.conversion_utils.convert_raw_fargate_profile_to_present(
                fargate_profile
            )
            resource_id = translated_resource.get("resource_id")
            result[resource_id] = {
                "aws.eks.fargate_profile.present": [
                    {parameter_key: parameter_value}
                    for parameter_key, parameter_value in translated_resource.items()
                ]
            }
        return result
    except Exception as e:
        result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)
        result["result"] = False
        return result
