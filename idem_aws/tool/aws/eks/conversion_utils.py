from collections import OrderedDict
from typing import Any
from typing import Dict


def convert_raw_fargate_profile_to_present(
    hub, raw_resource: Dict[str, Any]
) -> Dict[str, Any]:
    describe_parameters = OrderedDict(
        {
            "fargateProfileName": "name",
            "clusterName": "cluster_name",
            "podExecutionRoleArn": "pod_execution_role_arn",
            "status": "status",
        }
    )
    name = raw_resource.get("fargateProfileName")
    # resource id would be name-clusterName to maintain integrity as same fargate profile names can exist in different clusters
    resource_id = name + "-" + raw_resource.get("clusterName")
    translated_resource = {"resource_id": resource_id}
    for parameter_old, parameter_new in describe_parameters.items():
        if parameter_old in raw_resource:
            translated_resource[parameter_new] = raw_resource.get(parameter_old)
        if raw_resource.get("subnets"):
            translated_resource["subnets"] = raw_resource.get("subnets").copy()
        if raw_resource.get("selectors"):
            translated_resource["selectors"] = raw_resource.get("selectors").copy()
        if raw_resource.get("tags"):
            translated_resource["tags"] = raw_resource.get("tags").copy()
    return translated_resource
